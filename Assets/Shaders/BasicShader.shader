﻿Shader "Custom/basic" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_AmColor("Ambient Color", Color) = (1,1,1,1)
		_MySlider("This is a slider", Range(0,10))= 2.5
				
				
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		float4 _Color;
		float4 _AmColor;
		float _MySlider;
		

		// Use shader model 3.0 target, to get nicer looking lighting
		//#pragma target 3.0

		//sampler2D _MainTex;

		struct Input { 
			float2 uv_MainTex;
		};

		

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {


			// Albedo comes from a texture tinted by color
			
			float4 c = pow((_Color+_AmColor), _MySlider);
			
			
			o.Albedo = c.rgb;
			
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}