﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tarea : MonoBehaviour {
    
    Renderer r;

	// Use this for initialization
	void Start () {
        r = GetComponent<Renderer>();
        
	}

    // Update is called once per frame
    void Update()
    {
        float ambient = Mathf.PingPong(Time.time, 10);
        r.material.SetFloat("_AmbientColor", ambient);
    }
     
}
