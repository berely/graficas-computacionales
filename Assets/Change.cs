﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Change : MonoBehaviour
{

    public Material Flat;
    public Material Gira;
    public Material NormalMap;
    public Material Inicio;
    public Material Primero;
    public Material Textnormalmap;

    Material material1;
    Material material2;
    Renderer render;
   
    // Use this for initialization
    void Start()
    {
       
        Invoke("m1", 1.0f);
        
    }

    void m1()
    {
        GetComponent<Renderer>().material = Flat;

        Invoke("m2", 10.0f);
    }

    void m2()
    {
        GetComponent<Renderer>().material = Inicio;

        Invoke("m3", 5.0f);
    }

    void m3()
    {
        GetComponent<Renderer>().material = Gira;


        Invoke("m4", 5.0f);
    }
    void m4()
    {
        GetComponent<Renderer>().material = NormalMap;

        Invoke("m5", 5.0f);

    }
    void m5()
    {
        GetComponent<Renderer>().material = Textnormalmap;
        

        Invoke("m5", 5.0f);
    }


    private void Update()

    {
        
        if (Time.time >= 21)
        {

            transform.Rotate(new Vector3 (0f, 30f, 0f)*Time.deltaTime);


        }
        
       
    }

     
  


    





}
